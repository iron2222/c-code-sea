#include<stdio.h>



/*
int main()
{
   
    //输出一个整形数组中的是个元素
    int a[10];
    int i;
    printf("please enter ten integer number:\n");
    for ( i = 0; i < 10; i++)
    {
        scanf("%d",&a[i]);
    }
    for ( i = 0; i < 10; i++)
    {
        printf("a[%d] = %d\n",i,a[i]);
    }
    printf("\n");
    0


    return 0;
}
运行结果
please enter ten integer number:
1
2
3
4
5
6
7
8
9
a[0] = 0
a[1] = 1
a[2] = 2
a[3] = 3
a[4] = 4
a[5] = 5
a[6] = 6
a[7] = 7
a[8] = 8
a[9] = 9
*/

/*
//第二种方法，使用指针
int main()
{
    int a[10];
    int *p,i;
    printf("please enter ten integer number:");
    for ( i = 0; i < 10; i++)
    {
        scanf("%d",&a[i]);
    }
    for ( p = a; p < (a+10); p++)
    {
        printf("%d",*p);
    }
    
    
    return 0;
}

运行结果
please enter ten integer number:1
2
3
4
5
6
7
8
9
0
1234567890
*/
/*
int main()
{
    int a[10],i,*p = a;
    printf("please enter ten integer number:\n");
    for ( i = 0; i < 10; i++)   
    {
        scanf("%d",p++);
    }
    p = a;              //应为第一个for循环之后，p已经指向a[9]了，所以这里重新使p指向a[0]
    for ( i = 0; i < 10; i++,p++)
    {   
        printf("%d",*p);

    }
    printf("\n");

    return 0;
    
    
}
运行结果
please enter ten integer number:
1
2
3
4
5
6
7
8
9
0
1234567890
*/
/*
int main()
{

    int a[5] = {0,3,8,6,7};
    int *p = a;

    
    printf("*p++ = %d\n",*p++);         //*p++ = 0   是先*p，在对p++，所以下面可以看到，*p是数组第二个元素的值
    printf("*p = %d\n",*p);             //*p = 3     数组第二个元素的值
    p = a;                              //把p移到数组首地址
    printf("*(p++) = %d\n",*(p++));    //*(p++) = 0  和*p++效果一样
    printf("*p = %d\n",*p);             //*p = 3
    p = a;                              //把p移到数组首地址
    printf("*(++p) = %d\n",*(++p));     //*(++p) = 3   这个就不太一样了，是先p++,再*p,所以这个时候p指向的是数组第二个元素
    p = a;                              //把p移到数组首地址
    printf("++(*p) = %d\n",++(*p));     //++(*p) = 1   这个也不太一样，是先*p，再++ *p,就是说把指向的数组第一个元素+1
    

    return 0;
}
*/

