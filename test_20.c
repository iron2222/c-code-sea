
/*要求输出国际象棋棋盘*/

/*国际象棋棋盘由64个黑白相间的格子组成，分为8行*8列*/

#include<stdio.h> 

int main(void) 
{
    for(int i=1;i<9;i++)
    {
        if(i%2==0)
        {
            printf("■□■□■□■□\n"); 
        }else{
            printf("□■□■□■□■\n");
        }
    }
}

/*
测试效果
□■□■□■□■
■□■□■□■□
□■□■□■□■
■□■□■□■□
□■□■□■□■
■□■□■□■□
□■□■□■□■
■□■□■□■□
*/