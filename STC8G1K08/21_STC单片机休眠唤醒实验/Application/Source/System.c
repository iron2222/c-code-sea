//包含头文件
#include "system.h"

/***********************************************
函数名称：Delay_ms
功    能：STC8G系列单片机1ms延时程序
入口参数：ms:延时的毫秒数
返 回 值：无	
备    注：示波器实测：1.01ms，内部时钟：11.0592MHz           
************************************************/
void Delay_ms(unsigned int ms)
{
  unsigned int i;
  while( (ms--) != 0)
  {
    for(i = 0; i < 860; i++); 
  }             
}
/***********************************************
函数名称：System_Init
功    能：单片机系统初始化
入口参数：无
返 回 值：无	
备    注：STC-ISP下载软件中选择IRC频率：11.0592MHz	
          以下的函数和宏定义，请参考STC8G.H文件中的注释
************************************************/
void System_Init(void)
{	
	Delay_ms(300);									//延时等待上电稳定
	SetBits(P_SW2,EAXFR);						//允许访问特殊功能寄存器
	SetBits(HIRCCR,ENHIRC);					//使能内部高精度IRC	
	while (!(HIRCCR & HIRCST));			//等待时钟稳定
  ClrBits(CKSEL,Bits_ALL);      	//选择内部高精度IRC Bits_ALL使能所有位 
	ClrBits(MCLKOCR,Bits_ALL);     	//主时钟不输出 
	ClrBits(P_SW2,EAXFR);						//关闭访问特殊功能寄存器 
	
	//复位脚用作IO口P54
	ClrBits(RSTCFG,P54RST);
}