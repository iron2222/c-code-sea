 //包含该头文件，可以使用_nop_()函数
 #include <intrins.h>
 #include "tm1650.h"

//共阴数码管段码表
 unsigned char code DispCode[]={0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x6F,																			
                          //      0    1    2    3    4    5    6    7    8    9   
															  0x77,0x7C,0x39,0x5E,0x79,0x71,0x3D,0x76,0x74,0x30,	
													//      A	   b    C	   d	  E	   F    G	  H	    h	   I   	
																0x10,0x1E,0x38,0x54,0x5C,0x73,0x67,0x50,0x31,0x78,
													//      i    J    L	   n    o	   P    q	   r    R	   t     	
																0x3E,0x1C,0x40,0x48,0x08,0x00};
													//      U	   V   一    二	  _    灭
//显示缓冲区
unsigned char DispBuf[4]={0};

/***********************************************
函数名称：TM1650_Delay
功    能：TM1650延时程序
入口参数：us:延时的微秒数
返 回 值：无	
备    注：无
************************************************/
void TM1650_Delay(unsigned int us)
{
  while( us--)
  {
    _nop_();
  }             
}
/***********************************************
函数名称：TM1650_Start
功    能：TM1650开始信号
入口参数：无
返 回 值：无	
备    注：无
************************************************/
void TM1650_Start(void)
{
	Set_TM1650_SCL();	//拉高时钟线
	Set_TM1650_SDA();	//拉高数据线
	TM1650_Delay(1);  //延时
	Clr_TM1650_SDA();	//产生下降沿
	TM1650_Delay(1);  //延时
}
/***********************************************
函数名称：TM1650_Stop
功    能：TM1650停止信号
入口参数：无
返 回 值：无	
备    注：无
************************************************/
void TM1650_Stop(void)
{
	Set_TM1650_SCL();	//拉高时钟线
	Clr_TM1650_SDA();	//拉低数据线
	TM1650_Delay(1);  //延时
	Set_TM1650_SDA();	//拉高数据线
	TM1650_Delay(1);  //延时
}
/***********************************************
函数名称：TM1650_RecvACK
功    能：单片机从TM1650接收应答信号
入口参数：无
返 回 值：bit (0:ACK 1:NAK)	
备    注：无
************************************************/
bit TM1650_RecvACK(void)
{	
	bit ack;
	Clr_TM1650_SCL();    //拉低时钟线
	TM1650_Delay(1);
	Set_TM1650_SDA(); 	 //端口读之前先置高
  Set_TM1650_SCL();    //拉高时钟线
	TM1650_Delay(1);

  ack = TM1650_SDA;    //读应答信号    
  TM1650_Delay(1);     //延时

	Clr_TM1650_SCL();    //拉低时钟线
	TM1650_Delay(1);
	
  return ack;
}
/***********************************************
函数名称：TM1650_WriteByte
功    能：写一个字节数据到TM1650
入口参数：dat:写入的数据
返 回 值：无	
备    注：详见数据手册时序图。
************************************************/
void TM1650_SendByte(unsigned char dat)
{
	unsigned char i;
	for(i=0;i<8;i++)
	{		
		Clr_TM1650_SCL();    	//拉低时钟线
    TM1650_Delay(1);    	//延时
		if( (dat&0x80)==0x80 )//判断数据最高位
		{
			Set_TM1650_SDA(); 	//置高数据线
		}
		else
		{
			Clr_TM1650_SDA(); 	//置低数据线
		}
		dat <<= 1;           	//移出数据的最高位
		TM1650_Delay(1);    	//延时
    Set_TM1650_SCL();    	//拉高时钟线
    TM1650_Delay(1);    	//延时   
	}
	Clr_TM1650_SCL();    		//拉低时钟线
  TM1650_Delay(1);    		//延时
	
	TM1650_RecvACK();		 		//读取应答
}
/***********************************************
函数名称：TM1650_WriteReg
功    能：写一个字节数据到TM1650的寄存器，
入口参数：
					add：写入的寄存器地址	
					dat:写入的数据
返 回 值：无	
备    注：详见数据手册时序图。
************************************************/
void TM1650_WriteReg(unsigned char add,unsigned char dat)
{
	TM1650_Start();		   	//开始
	TM1650_SendByte(add);	//写入地址
	TM1650_SendByte(dat);	//写入数据
	TM1650_Stop();		   	//停止
}
/***********************************************
函数名称：TM1638_Clear
功    能：TM1638清除显示
入口参数：无
返 回 值：无	
备    注：无
************************************************/
void TM1650_Clear(void)
{	
	//写入数据0x00则全部熄灭不显示
	TM1650_WriteReg( DisAdd1,0x00 );
	TM1650_WriteReg( DisAdd2,0x00 );
	TM1650_WriteReg( DisAdd3,0x00 );
	TM1650_WriteReg( DisAdd4,0x00 );		
}
/***********************************************
函数名称：TM1650_Init
功    能：TM1650芯片初始化函数。
入口参数：无
返 回 值：无	
备    注：以下的函数和宏定义，请参考STC8G.H文件中的注释
************************************************/
void TM1650_Init(void)
{
	//延时等待上电稳定
	TM1650_Delay(1000);		  		  

	//初始化P36,P37口为开漏结构
	P3_Mode_OUT_OD(PIN_6|PIN_7); 
  //使能P36，P37内部上拉电阻	
	P3_PullUp_EN(PIN_6|PIN_7); 

	TM1650_Clear();			  	 				//清除显示	 	
	TM1650_WriteReg(DisMode,DisCtr);//显示开，设定亮度，八段显示
}
/***********************************************
函数名称：TM1650_ON
功    能：TM1650显示开函数。
入口参数：无
返 回 值：无	
备    注：无
************************************************/
void TM1650_ON(void)
{
	TM1650_WriteReg(DisMode,DisCtr);	 //显示控制，显示开，设定亮度，八段显示
}
/***********************************************
函数名称：TM1650_OFF
功    能：TM1650显示关闭函数。
入口参数：无
返 回 值：无	
备    注：无
************************************************/
void TM1650_OFF(void)
{
	TM1650_WriteReg(DisMode,DisOff);	 //显示控制，显示关	
}
/***********************************************
函数名称：ToDisplay__
功    能：数码管显示--
入口参数：无
返 回 值：无	
备    注：无
************************************************/
void ToDisplay__(void)
{
	DispBuf[0]=32;			//DispCode[32]:-
	DispBuf[1]=32;			//DispCode[32]:-
	DispBuf[2]=32;			//DispCode[32]:-
	DispBuf[3]=32;			//DispCode[32]:-

	//写入四位数码管显示数据
	TM1650_WriteReg( DisAdd1,DispCode[DispBuf[0]] );
	TM1650_WriteReg( DisAdd2,DispCode[DispBuf[1]] );
	TM1650_WriteReg( DisAdd3,DispCode[DispBuf[2]] );
	TM1650_WriteReg( DisAdd4,DispCode[DispBuf[3]] );	
}
/***********************************************
函数名称：ToDisplayHello
功    能：把欢迎信息放入显示缓冲区
入口参数：无
返 回 值：无	
备    注：在数码管中显示HELLO
************************************************/
void ToDisplayHello(void)
{
	DispBuf[0]=17;			//DispCode[17]:H
	DispBuf[1]=14;			//DispCode[14]:E
	DispBuf[2]=22;			//DispCode[22]:L
	DispBuf[3]=0;		 		//DispCode[0]:O

	//写入四位数码管显示数据
	TM1650_WriteReg( DisAdd1,DispCode[DispBuf[0]] );
	TM1650_WriteReg( DisAdd2,DispCode[DispBuf[1]] );
	TM1650_WriteReg( DisAdd3,DispCode[DispBuf[2]] );
	TM1650_WriteReg( DisAdd4,DispCode[DispBuf[3]] );	
}
/***********************************************
函数名称：ToDisplaySleep
功    能：把休眠信息放入显示缓冲区
入口参数：无
返 回 值：无	
备    注：在数码管中显示SLEEP
************************************************/
void ToDisplaySleep(void)
{
	DispBuf[0]=5;				//DispCode[5]:S
	DispBuf[1]=22;			//DispCode[22]:L
	DispBuf[2]=14;			//DispCode[14]:E
	DispBuf[3]=14;			//DispCode[14]:E

	//写入四位数码管显示数据
	TM1650_WriteReg( DisAdd1,DispCode[DispBuf[0]] );
	TM1650_WriteReg( DisAdd2,DispCode[DispBuf[1]] );
	TM1650_WriteReg( DisAdd3,DispCode[DispBuf[2]] );
	TM1650_WriteReg( DisAdd4,DispCode[DispBuf[3]] );	
}	