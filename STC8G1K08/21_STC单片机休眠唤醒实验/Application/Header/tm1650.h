#ifndef __TM1650_H__
#define __TM1650_H__

//包含头文件
#include "STC8G.h"

//定义硬件接口	     
sbit 	TM1650_SCL     =P3^6;	//时钟 
sbit 	TM1650_SDA     =P3^7;	//数据

//定义端口操作
#define Set_TM1650_SCL()	{TM1650_SCL=1;}	//端口置高
#define Clr_TM1650_SCL()	{TM1650_SCL=0;}	//端口置低

#define Set_TM1650_SDA()	{TM1650_SDA=1;}
#define Clr_TM1650_SDA()	{TM1650_SDA=0;}

//命令定义
#define	DisCtr			0x11		//显示控制，显示开，设定亮度，八段显示
#define	DisOff			0x10		//显示关闭
#define	DisMode			0x48		//模式命令

#define DisAdd1			0x68		//显示地址1
#define DisAdd2			0x6a		//显示地址2
#define DisAdd3			0x6c		//显示地址3
#define DisAdd4			0x6e		//显示地址4

//函数声明
void TM1650_Init(void);
void TM1650_ON(void);
void TM1650_OFF(void);
void ToDisplay__(void);
void ToDisplayHello(void);
void ToDisplaySleep(void);

#endif