#ifndef __SYSTEM_H__
#define __SYSTEM_H__

//包含系统头文件
#include "STC8G.h"

//函数声明
void System_Init(void);
void Delay_ms(unsigned int ms);

#endif