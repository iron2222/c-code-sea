#ifndef  __KEY_H__
#define	 __KEY_H__

 #include "stc8g.h"

 //按键所在端口定义
 sbit KEY6=P1^2;
 sbit KEY7=P3^4;
 sbit KEY10=P1^7;

 //函数声明
 void Key_Init(void);
 unsigned char Key_Scan(void);

#endif