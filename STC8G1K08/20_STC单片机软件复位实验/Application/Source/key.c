#include "key.h"

/***********************************************
函数名称：Key_Delayms
功    能：STC8系列单片机1ms延时程序
入口参数：ms:延时的毫秒数
返 回 值：无	
备    注：示波器实测：1.01ms，内部时钟：11.0592MHz           
************************************************/
void Key_Delayms(unsigned int ms)
{
  unsigned int i;
  while( (ms--) != 0)
  {
    for(i = 0; i < 860; i++); 
  }             
}
/***********************************************
函数名称：Key_Scan
功    能：按键扫描函数
入口参数：无
返 回 值：键值	
备    注：无
************************************************/
unsigned char Key_Scan(void)
{
	unsigned char KeyValue=0;
	//读入的端口先置高,准双向口做输入必须先置高
	KEY6=1;KEY7=1;KEY10=1;
	
	//有按键按下
	if(KEY6==0)
	{
		Key_Delayms(20);		//延时，防抖动     重新读取		
		if(KEY6==0)
		{
			//等待按键释放
			while(KEY6==0);
			KeyValue=6;	
		}
	}
	else if(KEY7==0)
	{
		Key_Delayms(20);		//延时，防抖动     重新读取		
		if(KEY7==0)
		{
			//等待按键释放
			while(KEY7==0);
			KeyValue=7;	
		}
	}
	else if(KEY10==0)
	{
		Key_Delayms(20);		//延时，防抖动     重新读取		
		if(KEY10==0)
		{
			//等待按键释放
			while(KEY10==0);
			KeyValue=10;	
		}
	}	
	return KeyValue;			
}
/***********************************************
函数名称：Key_Init
功    能：按键端口模式初始化
入口参数：无
返 回 值：无	
备    注：以下的函数和宏定义请参考STC8G.H文件中的注释
************************************************/
void Key_Init(void)
{
	//将 P12 P17 P34设置为准双向口
	P1_Mode_IO_PU(PIN_2|PIN_7);   
  P3_Mode_IO_PU(PIN_4); 	      
}