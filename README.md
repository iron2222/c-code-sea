# 内容相关：
## 文件夹C
这个文件夹显示了一个.c文件是怎么一步一步的变成可执行文件的
gcc -E -o hello.i hello.c
gcc -S -o hello.s hello.i
gcc -c -o hello.o hello.s
gcc -o hello hello.o
## 文件夹C_test
放一些C语言的训练小题目和知识点验证
## 文件夹C编写规范
有C语言编写规范和一些面试题，PDF建议好好看