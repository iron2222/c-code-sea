
/*
题目：企业发放的奖金根据利润提成。
    利润低于或等于10万元时，奖金可提10%；
    利润高于10万元，低于20万元时，低于10万元的部分按10%提成，高于10万元的部分，可提成7.5%；
    20万到40万之间时，高于20万元的部分，可提成5%；
    40万到60万之间时高于40万元的部分，可提成3%；
    60万到100万之间时，高于60万元的部分，可提成1.5%；
    高于100万元时，超过100万元的部分按1%提成。
    
    从键盘输入当月利润I，求应发放奖金总数？
*/


#include<stdio.h>

double calculateBonus(double money);
int main()
{
    double i, bonus;
    printf("你的净利润是(单位为：万)：\n");
    scanf("%lf",&i);
    bonus = calculateBonus(i);
    printf("提成为：bonus=%lf",bonus);
   
}
double calculateBonus(double money) {    
    if (money <= 10) {        
        return money * 0.1;    
    } else if(money <= 20) {        
        return (money - 10) * 0.075 + calculateBonus(10);    
    } else if(money <= 40) {         
        return (money - 20) * 0.05 + calculateBonus(20);    
    } else if(money <= 60) {        
        return (money - 40) * 0.03 + calculateBonus(40);    
    } else if(money <= 100) {        
        return (money - 60) * 0.015 + calculateBonus(60);         
    } else {        
        return (money - 100) * 0.01 + calculateBonus(100);    
    }
}