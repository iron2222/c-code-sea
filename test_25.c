#include<stdio.h>

int main()
{
    /*
    int a = 2;
    float *p;
    p = &a;
    printf("%f\n",*p);
    printf("%d\n",*p);
    */
/*
   int *p;
   *p = 100;
   printf("%d\n",*p);
*/
/*    
    int a = 10;
    int *p;
    p = &a;
    printf("%d\n",p);
    printf("%d\n",*p);

    程序运行结果
    6618692     
    10
*/

/*
    int a = 10;
    int *p;
    p = &a;
    printf("%o",p);

    程序运行结果
    31177104  //说是以八进制输出指针变量p的值，如果指向了a，那输出的就是a的地址
*/

/*
    //利用指针来排序
    int *p1,*p2,a,b;
    printf("please enter two integer number:\n");
    scanf("%d,%d",&a,&b);
    if (a<b)
    {
        p1 = &b;
        p2 = &a;
    }
    printf("a = %d,b = %d\n",a,b);
    printf("max = %d,min = %d\n",*p1,*p2);
    
    程序运行结果
    a = 23,b = 69           //说明并没有交换ab的值，而是分别把ab的值赋予了ab
    max = 69,min = 23
*/

/*
    //比较三个值得的大小，构造函数进行
    void exchange(int *ptr1,int *ptr2,int *ptr3);
    int a,b,c,*p1,*p2,*p3;
    printf("please enter three integer number:\n");
    scanf("%d,%d,%d",&a,&b,&c);
    p1 = &a;
    p2 = &b;
    p3 = &c;
    exchange(p1,p2,p3);
    printf("the order is :%d,%d,%d\n",a,b,c);

    运行结果
    please enter three integer number:
    16,58,-99
    the order is :58,16,-99
*/   
    return 0;
}

void exchange(int *ptr1,int *ptr2,int *ptr3)      //定义一个将三个变量的值进行交换的函数，把最大值给p1
{
    void swap(int *pt1,int *pt2);
    if (*ptr1<*ptr2)
    {
        swap(ptr1,ptr2);
    }
    if (*ptr1<*ptr3)
    {
        swap(ptr1,ptr3);
    }
    if (*ptr2<*ptr3)
    {
        swap(ptr2,ptr3);
    }
}


void swap(int *pt1,int *pt2)               //定义一个将两个变量进行交换的函数
{
    int temp;
    temp = *pt1;
    *pt1 = *pt2;
    *pt2 = temp;
}