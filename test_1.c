/*
输入三个数字，输出他们的最大值
*/

#include<stdio.h>

int main()
{
    int a,b,c,max;
    printf("请输入三个数字：\n");
    scanf("%d,%d,%d",&a,&b,&c);

    max = a;
    if(max<b) max = b;
    if(max<c) max = c;

    printf("最大值为：%d\n",max);
    return 0;
}