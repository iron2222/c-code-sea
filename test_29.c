/*
有五个学生，每个学生有3门课的成绩
从键盘输入以上数据（包括学生号，姓名，三门课成绩），计算出平均成绩，况原有的数据和计算出的平均分数存放在磁盘文件"stud"中。
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct {
    int ID;
    int math;
    int English;
    int C;
    int average;
    char name[20];
}Stu;

int main()
{
    FILE*fp;
    Stu stu[5];
    int i,average=0;
    printf("请输入5个同学的的信息：学生号，姓名，3门成绩\n");
    for ( i = 0; i < 5; i++)
    {
        scanf("%d %s %d %d %d",&(stu[i].ID),&(stu[i].name),&(stu[i].math),&(stu[i].English),&(stu[i].C));
        stu[i].average=(stu[i].math+stu[i].English+stu[i].C)/3;
    }

    if ((fp = fopen("stud","w")) == NULL)
    {
        printf("error:cannot open file!\n");
        exit(0);
    }

    for ( i = 0; i < 5; i++)
    {
        fprintf(fp,"%d %s %d %d %d %d\n",stu[i].ID,stu[i].name,stu[i].math,stu[i].English,stu[i].C,stu[i].average);

    }
    
    fclose(fp);
    return 0;

    /*
    运行结果
    请输入5个同学的的信息：学生号，姓名，3门成绩
    1 a 60 70 80
    2 b 60 80 75
    3 c 60 90 86
    4 d 55 95 60
    5 e 65 99 80
    PS D:\c\Cpractical\C课本例题训练> cat .\stud
    1 a 60 70 80 70
    2 b 60 80 75 71
    3 c 60 90 86 78
    4 d 55 95 60 70
    5 e 65 99 80 81
    */
    
    
}

