
#include<reg52.h> 

#define DataPort P0//定义的数据端口


void Delay(unsigned int t); 
sbit IO_IN=P2^0;
sbit LATCH1=P2^1;//位锁存
sbit LATCH2=P2^2;//段锁存

unsigned char code dofly_table[10]={0xc0,0xf9,0xa4,0xb0,0x99,0x92,0x82,0xf8,0x80,0x90};   

unsigned char code dofly_WeiMa[]={0xfe,0xfd,0xfb,0xf7,0xef,0xdf,0xbf,0x7f};
//分别对应相应的数码管点亮,即位码
unsigned char code dofly_DuanMa[]={0x3f,0x06,0x5b,0x4f,0x66,0x6d,0x7d,0x07,0x7f,0x6f};
// 显示段码值01234567

void main (void)
{           
	unsigned char i = 0; 	
		
	/*灯光测试部分*/	    
	/*for(i=0;i<8;i++)   
		{
				Delay(10000);
				P1<<=1;
		}
	P1 = 0xff; 
	for(i=0;i<8;i++)   
		{
				Delay(10000);
				P1>>=1;
		}
	P1 = 0xff;
	Delay(50000);
	P1 = 0;
	Delay(50000);
	P1 = 0xff;
	Delay(50000);
	P1 = 0;
	Delay(50000);
	P1 = 0xff;*/
		
	 /*数码管测试部分*/ 
	/*for(i=0;i<10;i++)   
  {
  P0=dofly_table[i]; 
  Delay(20000);      
  }
	P0=0x00;
	Delay(50000);
	P0=0xFF;
	Delay(50000);
	P0=0x00;
	Delay(50000);
	P0=0xFF;*/
	

	
while (1)        
  {
	   for(i=0;i<8;i++)
		{
		 DataPort=dofly_WeiMa[i]; //取位码 
     LATCH1=1;     //位锁存
     LATCH1=0;

     DataPort=dofly_DuanMa[i]; //取显示数据，段码
     LATCH2=1;     //段锁存
     LATCH2=0;

	   Delay(6000); //扫描间隙延时，时间太长会闪烁，太短会造成重影
	  
	 } 	
		/*多位数码管测试1*/
		/*DataPort = 0xaa;//确定位置，用哪一个数码管
		LATCH1=1;//这步其实没什么用处
		LATCH1=0;//锁住位置，其实真正起作用的就是这部分，给一个低电平
		DataPort = 0x4f;//确定数据大小
		LATCH2=1;
		LATCH2=0;//锁住大小
		*/
			
			/*独立按键功能测试*/
			/*switch(P3)                           
       {
				case 0xfe:P0=dofly_table[1];break;
				case 0xfd:P0=dofly_table[2];break;
				case 0xfb:P0=dofly_table[3];break;
				case 0xf7:P0=dofly_table[4];break;
				case 0xef:P0=dofly_table[5];break;
				case 0xdf:P0=dofly_table[6];break;
				case 0xbf:P0=dofly_table[7];break;
				case 0x7f:P0=dofly_table[8];break;
				default:break;    
	   }*/
		
		
			/*在这里发现，这些引脚在平时都是保持高电平状态，而且看原理图也可以相互印证独立按键按下的时候是拉低的，给低电平*/
		  /*if(IO_IN==1)
				P0=0x89;	//H
			else
				P0=0xc7;*/  
			
  }
}

void Delay(unsigned int t)
{
 while(--t);
}