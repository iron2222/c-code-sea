#include<stdio.h>

int main()
{
    struct node {
        int a;
        int b;
        int c;
    }; // 结构体的成员在内存中的地址是按照他们定义的位置顺序依次增长的

    struct node s = { 3, 5, 6 };
    struct node *pt = &s;
    printf("%d\n" ,  *(int*)pt);  
    printf("%d\n" ,  *pt);  
    printf("%d\n" ,  (int*)pt);  
    printf("%d\n" ,  pt);  

    return 0;
}
/*
3
6618656
6618684
6618684
*/