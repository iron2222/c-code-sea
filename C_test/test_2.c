#include <stdio.h>
double calculate_bonus(double money);

int main(){
    double i,bonus;
    printf("你的净利润是（万元）：\n");
    scanf("%lf",&i);
    bonus = calculate_bonus(i);
    printf("提成为：bonus=%lf",bonus);

}

double calculate_bonus(double money){
    if (money <= 10)
    {
        return money * 0.1;
    }else if(money <= 20){
        return (money - 10) * 0.075 + calculate_bonus(10);
    }else if (money <= 40){
        return (money - 20) * 0.05 + calculate_bonus(20);
    }else if (money <= 60){
        return (money - 40) * 0.03 + calculate_bonus(40);
    }else if (money <=100){
        return (money - 60) * 0.015 + calculate_bonus(60);
    }else{
        return (money - 100) * 0.01 + calculate_bonus(100);
    }
    
}