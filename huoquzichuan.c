#include<stdio.h>
#include <string.h>
#include <stdlib.h>

/*                  构造一个函数，获取字符串的子串                   */

//strSource原始字符串；uStartPos字符串起始位置；uEndPos字符串结束为止           //const初始化的变量，是不可以修改的
char* GetSubstring(const char *strSource, const unsigned int uStartPos, const unsigned int uEndPos)
{
	unsigned int uLen = strlen(strSource);  //字符串长度
	if (uLen == 0)                          //如果字符串长度为0，返回为空
	{
		return "";
	}
	char *strTemp = (char *)malloc(uLen+1); //声明一个字符串类型的指针strTemp，数组指针(char *)malloc(uLen+1)，这个malloc好像是获取空间的作用
	memset(strTemp, 0, uLen+1);             //类似于初始化的作用，给这个指针定义空间大小uLen+1，并填充某个给定的值0
	strcpy(strTemp, strSource);             //把原始字符串复制到这个指针中
 
	if(uStartPos > uLen)                    //如果字符串结束位置大于字符串长度，就返回空
	{
		free(strTemp);
		return "";
	}
 
	uLen = uEndPos - uStartPos;             //字符串长度=字符串结束为止-字符串开始位置
	char *strSub = (char *)malloc(uLen+1);  //再次声明一个*strSub
	memset(strSub, 0, uLen+1);              //和上面一样
 
	unsigned int i;                          
	for(i=0; i<=uLen; i++)                  //循环获取子串
	{
		strSub[i] = strTemp[uStartPos + i];
	}
	strSub[i] = '\0';                       //最后一个是\0
 
	free(strTemp);
 
	return strSub;
}

int main()
{
    char data_park[128]={"01 10 B6 29 0C 8B 00 00 00 00 08 03 1E 00 B0"};
    printf("字符串前四位是：%s",GetSubstring(data_park,0,4));
    return 0;
}
