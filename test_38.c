#include<stdio.h>

void main(void)
{
    int a[5] = {1, 2, 3, 4, 5};

    printf("%d\n",&a);//6618672
    printf("%d\n",*a);//1

    printf("%d\n",(int *)a);//6618672
    printf("%d\n",*(int *)a);//1

    printf("%d\n",&a+1);//6618692
    printf("%d\n",(int *)(&a+1));//6618692
    printf("%d\n",*(int *)(&a+1));//0
    printf("%d\n",*(&a+1));//6618692

    printf("%d\n",*(a+1));//2


    int *ptr =  (int *)(&a + 1); // &a取址后以a[0-4]整个数组为步进，a以a[0]也就是int为步进
    printf("%d\n",*ptr);//0
    printf("%d\n",&ptr);//6618664
    printf("%d\n",*ptr+1);//1
    printf("%d\n",*(ptr+1));//0
    printf("%d\n",*(ptr-1));//5

    printf("%d %d" , *(a + 1), *(ptr - 1));//2 5
}

/*
a的类型是一个整型数组,它有5个成员
&a的类型是一个整型数组的指针
所以&a + 1指向的地方等同于 a[6]
所以*(a+1) 等同于a[1]
ptr等同 a[6], ptr-1就等同与a[5]
*/