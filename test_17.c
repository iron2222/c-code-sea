
/*
用*号输出字母C的图案。
*/

/*
我在这里想练一下函数的构造，所以就写了一个函数
*/
#include <stdio.h>

void p(int x)
{
  int i;
  for(i=1;i<=x;i++) 
  {
    printf("*");
  }
    printf("\n");
}

int main()
{ 
    printf("\n");
    p(8);p(1);p(1);p(1);p(1);p(8);
    printf("\n");
    return 0;
}

/*
程序测试：
********
*
*
*
*
********   

*/